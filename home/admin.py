from django.contrib import admin
from .models import *
# Register your models here.
@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ['ismi', 'emaili', 'telefon_raqami','web_sayti', 'content']  
    search_fields = ['ismi', 'emaili', 'telefon_raqami','web_sayti', 'content']  

class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'created_at']  
    search_fields = ['title', 'author__username']  
    list_filter = ['created_at'] 
