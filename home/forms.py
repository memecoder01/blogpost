from django import forms
from .models import Post
from .models import Contact

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'content')


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['ismi', 'emaili', 'telefon_raqami', 'web_sayti', 'content']